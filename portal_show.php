<?php
if(!isset($_GET['id'])){
    header('Location:index.php');
}
require_once 'connection_db.php';
require_once 'Class\Publication.php';
require_once 'Class\PublicationsWriter.php';

$id = (int)$_GET['id'];
$portal = Publication::create($id, $pdo);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Information</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<div>
    <?php include "header.html";?>  <!--header block-->
</div>

<div style="text-align: center;">
    <h1><?=$portal->getTitle();?></h1>
    <h3><?=$portal->getLongText();?></h3>
    <h3><a href="index.php">Back</a></h3>
</div>

<div>
    <?php include "footer.html";?>  <!--footer block-->
</div>

</body>
</html>


