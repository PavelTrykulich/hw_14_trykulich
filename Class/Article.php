<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 06.05.2018
 * Time: 15:22
 */

class Article extends Publication
{
    protected $author = ' ';

    function __construct($id,$title, $publicationType, $introductory, $long_text, $author)
    {
        parent::__construct($id,$title, $publicationType, $introductory, $long_text);
        $this->author = $author;
    }
    public function getAuthor(){
        return $this->author;
    }

}