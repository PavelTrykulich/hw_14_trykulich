<?php
/**
 * Created by PhpStorm.
 * User: Паша
 * Date: 02.05.2018
 * Time: 13:19
 */
$publicationsArray = [
       ['title' => 'Hacker',
        'publicationType'=>'Article',
        'introductory'=>'hacking system',
        'long_text'=> 'The hacker broke the bank through sql script',
        'author' => 'Harper O.K.'
       ],
        ['title' => 'Robot',
         'publicationType'=>'News',
        'introductory'=>'All about robots',
        'long_text'=> 'The development of robotics is gaining momentum',
        'source_publication' => 'index'
        ],
        ['title' => 'Toaster',
        'publicationType'=>'Article',
        'introductory'=>' The smart toaster',
        'long_text'=> 'In China, they created a smart toaster',
        'author' => 'Franko T.I.'
        ],
        ['title' => 'Programming',
        'publicationType'=>'News',
        'introductory'=>'How to become a programmer',
        'long_text'=> 'Top Tips for Beginners',
        'source_publication' => 'hub'
        ],
        ['title' => 'Weather',
        'publicationType'=>'Article',
        'introductory'=>'Weather today',
        'long_text'=> 'Sunny no precipitations',
        'author' => 'Ivanova I.I.'
        ],
        ['title' => 'ONAT',
        'publicationType'=>'News',
        'introductory'=>'studying in ONAT',
        'long_text'=> 'Specialties and career development opportunities',
        'source_publication' => 'dekanat'
        ]
];
